// import 'package:flutter/material.dart';
// import 'package:intro_slider/intro_slider.dart';
// import 'package:intro_slider/slide_object.dart';
//
//
// class IntroSliderPage extends StatefulWidget {
//
//
//   @override
//   _IntroSliderPageState createState() => _IntroSliderPageState();
// }
//
// class _IntroSliderPageState extends State<IntroSliderPage> {
//
//   List<Slide> slides = [];
//
//   late Function goToTab;
//
//   @override
//   void initState() {
//     // TODO: implement initState
//     super.initState();
//
//
//     slides.add(new Slide(
//       backgroundImage:"assets/images/bg.png" ,
//
//       title: "Lorem lpsum",
//       styleTitle: TextStyle(
//         color: Colors.black,
//         fontFamily: "Open Sans ExtraBold Regular",
//         fontSize: 20.0,
//       ),
//       description: "Lorem lpsum is simply dummy text of the printing and typesetting industry. Lorem lpsum has been the industry's standard dummy text ever since the 1500s",
//       styleDescription: TextStyle(
//         color: Colors.black,
//         fontFamily: "Open Sans Light Regular",
//         fontSize: 15.0,
//       ),
//       pathImage: "assets/images/intro/graphic_1.png",
//
//     ));
//
//     slides.add(new Slide(
//         title: "Why do we use it?",
//       styleTitle: TextStyle(
//         color: Colors.black,
//         fontFamily: "Open Sans ExtraBold Regular",
//         fontSize: 20.0,
//       ),
//         description:"It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem lpsum is that it. ",
//       styleDescription: TextStyle(
//         color: Colors.black,
//         fontFamily: "Open Sans Light Regular",
//         fontSize: 15.0,
//       ),
//       pathImage: "assets/images/intro/graphic_2.png",
//     ));
//
//     slides.add(new Slide(
//         title: "Where does it come from?",
//       styleTitle: TextStyle(
//         color: Colors.black,
//         fontFamily: "Open Sans ExtraBold Regular",
//         fontSize: 20.0,
//       ),
//         description:"Contrary to popular belief, Lorem lpsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old.",
//       styleDescription: TextStyle(
//         color: Colors.black,
//         fontFamily: "Open Sans Light Regular",
//         fontSize: 15.0,
//       ),
//       pathImage: "assets/images/intro/graphic_3.png",
//
//
//     ));
//   }
//
//   void onDonePress() {
//     // Back to the first tab
//     this.goToTab(0);
//   }
//   void onTabChangeCompleted(index) {
//     // Index of current tab is focused
//     print(index);
//   }
//   Widget renderNextBtn() {
//     return Icon(
//       Icons.next_plan_sharp,
//       color: Colors.green,
//       size: 35.0,
//
//     );
//   }
//   Widget renderDoneBtn() {
//     return Icon(
//       Icons.done,
//       color: Colors.red,
//
//     );
//   }
//   Widget renderSkipBtn() {
//     return Icon(
//       Icons.skip_next,
//       color: Colors.blue,
//     );
//   }
//   ButtonStyle myButtonStyle() {
//     return ButtonStyle(
//       shape: MaterialStateProperty.all<OutlinedBorder>(StadiumBorder()),
//
//     );
//   }
//   List<Widget> renderListCustomTabs() {
//     List<Widget> tabs = [];
//     for (int i = 0; i < slides.length; i++) {
//       Slide currentSlide = slides[i];
//       tabs.add(Container(
//         width: double.infinity,
//         height: double.infinity,
//         child: Container(
//
//           child: ListView(
//             children: <Widget>[
//               GestureDetector(
//                   child: Image.asset(
//                     currentSlide.pathImage.toString(),
//                     width: 200.0,
//                     height: 200.0,
//                     fit: BoxFit.contain,
//                   )),
//               Container(
//                 child: Text(
//                   currentSlide.title.toString(),
//                   style: currentSlide.styleTitle,
//                   textAlign: TextAlign.center,
//                 ),
//                 margin: EdgeInsets.only(top: 20.0),
//               ),
//               Container(
//                 child: Text(
//                   currentSlide.description.toString(),
//                   style: currentSlide.styleDescription,
//                   textAlign: TextAlign.center,
//                   maxLines: 5,
//                   overflow: TextOverflow.ellipsis,
//                 ),
//                 margin: EdgeInsets.only(top: 20.0),
//               ),
//               Container(
//                 child: Image.asset(currentSlide.backgroundImage.toString(),
//
//                   fit: BoxFit.cover,
//                 ),
//               ),
//
//             ],
//           ),
//         ),
//       ));
//     }
//     return tabs;
//   }
//   @override
//   Widget build(BuildContext context) {
//     var dotSliderAnimation;
//     return new IntroSlider(
//
//
//       // Skip button
//       renderSkipBtn: this.renderSkipBtn(),
//       skipButtonStyle: myButtonStyle(),
//       // Next button
//       renderNextBtn: this.renderNextBtn(),
//       nextButtonStyle: myButtonStyle(),
//       // Done button
//       renderDoneBtn: this.renderDoneBtn(),
//       onDonePress: this.onDonePress,
//       doneButtonStyle: myButtonStyle(),
//
//       // Dot indicator
//       colorDot: Color(0x33FFA8B0),
//       colorActiveDot: Color(0xffFFA8B0),
//       sizeDot: 13.0,
//
//
//
//       // Tabs
//       listCustomTabs: this.renderListCustomTabs(
//
//       ),
//       backgroundColorAllSlides: Colors.white,
//
//       refFuncGoToTab: (refFunc) {
//         this.goToTab = refFunc;
//       },
//       // Behavior
//       scrollPhysics: BouncingScrollPhysics(),
//       // Show or hide status bar
//       hideStatusBar: true,
//       // On tab change completed
//       onTabChangeCompleted: this.onTabChangeCompleted,
//
//
//     );
//
//   }
//
// }
