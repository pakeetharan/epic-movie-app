import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:movie/create_account/create_acc.dart';
import 'package:movie/palatte.dart';
import 'package:movie/widgets/background_image.dart';

class LoginPage extends StatelessWidget {
  const LoginPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        BackgroundImage(),
        Scaffold(
          backgroundColor: Colors.transparent,


          body: SafeArea(
            child: Column(
            children: [ SizedBox(
              height: 130,
            ),

              Container(
                padding: const EdgeInsets.symmetric(
                  horizontal: 40, vertical: 10,
                ),
                child: Column(
                  children:[
                    Column(
                  children: [
                    TextInput(
                      hint: "Username",
                      inputAction: TextInputAction.next,
                    ),
                    PasswordInput(
                      hint: "Password",
                      inputAction: TextInputAction.done,
                    ),

                  ],
                  ),
                    Column(
                      children: [
                        Container(
                          width: double.infinity,
                          decoration: BoxDecoration(
                            image: DecorationImage(
                              image: AssetImage("assets/images/btn_1 (1).png"),
                            ),
                          ),
                          child: TextButton(onPressed: (){},
                          child: Text('Login',
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: 20.0,),
                          ),
                          ),
                        )
                      ],
                    ),
                    Column(
                      children: [ SizedBox(
                        height: 90,
                      ),

                        Container(
                          width: double.infinity,
                          decoration: BoxDecoration(
                            image: DecorationImage(
                              image: AssetImage("assets/images/btn_1 (1).png"),
                            ),
                          ),
                          child: TextButton(onPressed: (){
                            Navigator.push(context,MaterialPageRoute(
                                builder: (context)=> const CreateAccount()));
                          },
                            child: Text('Create Account',
                              style: TextStyle(
                                color: Colors.white,
                                fontSize: 20.0,),
                            ),
                          ),
                        )
                      ],
                    )
                  ],
                ),
              ),
            ],
              
            ),
          ),
        ),
      ],
    );
  }
}

class TextInput extends StatelessWidget {
  const TextInput({
    Key? key,
    required this.hint,

    required this.inputAction,

  }) : super(key: key);

  final String hint;

  final TextInputAction inputAction;

  @override
  Widget build(BuildContext context) {
    return Padding(padding: const EdgeInsets.symmetric(vertical: 12.0),
    child: Container(
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(16),
      ),
      child: TextField(

        decoration: InputDecoration(
          contentPadding:
          const EdgeInsets.symmetric(
            vertical: 5, horizontal: 10,
          ),
          border: InputBorder.none,
          hintText: hint,
          hintStyle: kBodyText,
        ),
        style: kBodyText,
        maxLength: 20,
        textInputAction: inputAction,
      ),

    ),
    );
  }
}

class PasswordInput extends StatelessWidget {
  const PasswordInput({
    Key? key,
    required this.hint,

    required this.inputAction,

  }) : super(key: key);

  final String hint;

  final TextInputAction inputAction;

  @override
  Widget build(BuildContext context) {
    return Padding(padding: const EdgeInsets.symmetric(vertical: 12.0),
      child: Container(
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(16),
        ),
        child: TextField(
          decoration: InputDecoration(
            contentPadding:
            const EdgeInsets.symmetric(
              vertical: 2, horizontal: 5,
            ),
            border: InputBorder.none,
            hintText: hint,
            hintStyle: kBodyText,
          ),
          style: kBodyText,
          obscureText: true,
          textInputAction: inputAction,
          maxLength: 20,

        ),

      ),
    );
  }
}
